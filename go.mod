module github.com/viktorburka/chatty

require (
	github.com/viktorburka/saysomething/v2 v2.0.0
	golang.org/x/text v0.3.0 // indirect
	rsc.io/sampler v1.99.99 // indirect
)
